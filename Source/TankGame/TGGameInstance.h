// Archi

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "TGGameInstance.generated.h"

class UProjectileManager;

/**
 * 
 */
UCLASS()
class TANKGAME_API UTGGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	virtual void Init() override;
};
