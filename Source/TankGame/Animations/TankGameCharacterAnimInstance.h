// Archi

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "TankGameCharacterAnimInstance.generated.h"


class ATGCharacter;
class UCharacterMovementComponent;

/**
 * 
 */
UCLASS()
class TANKGAME_API UTankGameCharacterAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

protected:
	UTankGameCharacterAnimInstance();
};
