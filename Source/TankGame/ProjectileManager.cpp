// Archi


#include "ProjectileManager.h"

#include "Actors/ProjectileBase.h"


void UProjectileManager::Initialize()
{
	for(int32 counter = 0 ; counter < BaseNumberOfProjectiles ; ++counter)
	{
		FreeProjectiles.Add(SpawnProjectile());
	}
}

void UProjectileManager::ProjectileManagerTick(const float DeltaTime)
{
}

AProjectileBase* UProjectileManager::GetProjectile()
{
	AProjectileBase* BulletToFire;
	
	if(FreeProjectiles.Num() < 1)
	{
		BulletToFire = SpawnProjectile();
	}
	else
	{
		BulletToFire = FreeProjectiles.Pop();
	}
	
	// Binding to delegate after spawning seems to be too early so for now it has to be done here.
	// BeginPlay didn't fire yet??
	if(!BulletToFire->OnProjectileHitDelegate.Contains(this, FName("OnProjectileHit")))
	{
		BulletToFire->OnProjectileHitDelegate.AddDynamic(this, &UProjectileManager::OnProjectileHit);
	}
	if(!BulletToFire->OnProjectileTimeout.Contains(this, FName("OnProjectileTimeout")))
	{
		BulletToFire->OnProjectileTimeout.AddDynamic(this, &UProjectileManager::OnProjectileTimeout);
	}
	
	EngagedProjectiles.Add(BulletToFire);

	return BulletToFire;
}


AProjectileBase* UProjectileManager::SpawnProjectile() const
{
	return GetWorld()->SpawnActor<AProjectileBase> (ProjectileBaseClass, FVector(0.f), FRotator(0.f));
}

void UProjectileManager::OnProjectileHit(AProjectileBase* Projectile, APawn* ProjectileInstigator, const FHitResult& ImpactResult)
{
	const int32 ProjectileIndex = EngagedProjectiles.Find(Projectile);
	if(ProjectileIndex == INDEX_NONE)
		return;
	
	// RemoveAtSwap avoids potential movement of whole array because we deleted 1 item.
	EngagedProjectiles.RemoveAtSwap(ProjectileIndex, 1, false);
	
	FreeProjectiles.AddUnique(Projectile);
}

void UProjectileManager::OnProjectileTimeout(AProjectileBase* Projectile, APawn* ProjectileInstigator)
{
	const int32 ProjectileIndex = EngagedProjectiles.Find(Projectile);
	if(ProjectileIndex == INDEX_NONE)
		return;	

	EngagedProjectiles.RemoveAtSwap(ProjectileIndex, 1, false);

	FreeProjectiles.AddUnique(Projectile);
}