// Archi

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ProjectileFiringComponent.generated.h"

class UProjectileManager;

// Logic responsible for firing projectiles (executed by server only).
// NOTE: Automatically gets SkeletalMeshComponent from Owner to work on when trying to fire a projectile.
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TANKGAME_API UProjectileFiringComponent : public UActorComponent
{
	GENERATED_BODY()

	// TODO: Add firing types (auto, semi-auto, etc..)
	
public:// Engine methods
	UProjectileFiringComponent();
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	UFUNCTION(BlueprintCallable)
	void TryToFire_DestinationLocation(const FVector& ProjectileFinalDestination_W);
	UFUNCTION(BlueprintCallable)
	void TryToFire_Rotation(const FRotator& ProjectileRotation);
	UFUNCTION(BlueprintCallable)
	void TryToFire_SocketDecisive();
	
protected:
	UFUNCTION(BlueprintCallable)
	void FireProjectile(const FVector& StartLocation_W, const FRotator& StartRotation_W) const;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings | Setup")
	FName FireSocketName = FName();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings | Setup")
	float ProjectileInitialVelocity {100};
	
protected:// Debugging variables.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Debug | Variables")
	UProjectileManager* ProjectileManager {nullptr};

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Debug | Variables")
	const USkeletalMeshComponent* OwnerSkeletalMeshComponent {nullptr};
	
protected:
	virtual void BeginPlay() override;

};
