// Archi


#include "WeaponManagerComponent.h"

#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "ProjectileFiringComponent.h"

#include "TankGame/Actors/Pawns/Characters/TGCharacter.h"
#include "Camera/CameraComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "TankGame/WeaponInterface.h"


UWeaponManagerComponent::UWeaponManagerComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

	SetIsReplicatedByDefault(true);
}

void UWeaponManagerComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UWeaponManagerComponent::EquipWeapon(UProjectileFiringComponent* WeaponFiringComponent, bool bDisableCollision)
{
	if(UKismetSystemLibrary::DoesImplementInterface(WeaponFiringComponent->GetOwner(), UWeaponInterface::StaticClass()))
	{
		IWeaponInterface::Execute_OnWeaponEquipped(WeaponFiringComponent->GetOwner(), GetOwner(), bDisableCollision);
	}
	
	EquippedWeaponFiringComponent = WeaponFiringComponent;
	
	if(APawn* OwningPawn = Cast<APawn> (GetOwner()))
	{
		if(OwningPawn->IsPawnControlled() && OwningPawn->InputComponent)
		{
			SetupInput(OwningPawn);
		}
		
		OwningPawn->ReceiveRestartedDelegate.AddDynamic(this, &UWeaponManagerComponent::OnPawnPossessed);
	}
}
void UWeaponManagerComponent::OnPawnPossessed(APawn* ChangedPawn)
{
	if(GetOwner() != ChangedPawn)
	{
		return;
	}

	SetupInput(ChangedPawn);
}

void UWeaponManagerComponent::SetupInput(APawn* PawnToSetupInput)
{
	if(!PawnToSetupInput->IsLocallyControlled())
	{
		return;
	}
	
	const auto EnhancedInputLocalPlayerSubsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>
	(
		GetWorld()->GetFirstPlayerController()->GetLocalPlayer()
	);
	EnhancedInputLocalPlayerSubsystem->AddMappingContext(WeaponMappingContext, WeaponMappingContextPriority);

	RegisterInputActions(PawnToSetupInput);
}
void UWeaponManagerComponent::RegisterInputActions(const APawn* OwningPawn)
{
	if(ShootInputAction && OwningPawn && OwningPawn->IsLocallyControlled())
		if(UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent> (OwningPawn->InputComponent))
		{
			EnhancedInputComponent->BindAction(ShootInputAction, ETriggerEvent::Started, this, &UWeaponManagerComponent::ServerTryToShoot);
		}
}

void UWeaponManagerComponent::ServerTryToShoot_Implementation()
{
	if(!EquippedWeaponFiringComponent)
	{
		UE_LOG(LogTemp, Warning, TEXT("%s - EquippedWeapon variable is null."), *GetOwner()->GetName());
		return;
	}

	// TODO: Leave TryToFire distinction to ProjectileFiringComponent.
	if(bUseSocketToDetermineProjectileSetup)
	{
		EquippedWeaponFiringComponent->TryToFire_SocketDecisive();
		return;
	}

	// TODO: Move check to constructor.
	if(!UKismetSystemLibrary::DoesImplementInterface(GetOwner(), UPlayerPawnInterface::StaticClass()))
	{
		UE_LOG(LogTemp, Error, TEXT("WeaponManagerComponent owner doesn't inherit from PlayerPawnInterface. Firing is impossible."))
		return;
	}
	IPlayerPawnInterface* OwningPlayerPawn = Cast<IPlayerPawnInterface> (GetOwner());

	const UCameraComponent* Camera = OwningPlayerPawn->GetPlayerCamera();
	const FVector LineTraceStartLocation_WS = Camera->GetComponentToWorld().GetLocation();
	
	const FVector CameraDirection = Camera->GetComponentRotation().Vector();
	const FVector InteractionVector = CameraDirection * 100000;
	const FVector LineTraceEndLocation_WS = InteractionVector + Camera->GetComponentToWorld().GetLocation();

	FHitResult OutHitResult;
	FCollisionQueryParams CollisionQueryParams;
	CollisionQueryParams.AddIgnoredActor(GetOwner());

	if(bDrawDebugs)
		DrawDebugLine(GetWorld(), LineTraceStartLocation_WS, LineTraceEndLocation_WS, FColor::Red, true);
	
	const bool bLineTraceSucceeded = GetWorld()->LineTraceSingleByChannel
	(
		OutHitResult, LineTraceStartLocation_WS, LineTraceEndLocation_WS,
		ECC_Visibility,
		CollisionQueryParams
	);

	if(bLineTraceSucceeded)
	{
		EquippedWeaponFiringComponent->TryToFire_DestinationLocation(OutHitResult.Location);
	}
	else
	{
		// Trace failed to hit so probably player aims into the air in this case all we need is approximate rotation to fire the projectile.
		// TODO: Use weapon socketRotation instead?
		EquippedWeaponFiringComponent->TryToFire_Rotation(Camera->GetComponentRotation());
	}
}


