// Archi


#include "ProjectileFiringComponent.h"

#include "TankGame/TGGameMode.h"
#include "TankGame/ProjectileManager.h"
#include "TankGame/Actors/ProjectileBase.h"


UProjectileFiringComponent::UProjectileFiringComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

	SetIsReplicatedByDefault(true);
}


void UProjectileFiringComponent::BeginPlay()
{
	Super::BeginPlay();

	if(GetOwner()->HasAuthority())
		if(const ATGGameMode* TGGameMode = Cast<ATGGameMode> (GetWorld()->GetAuthGameMode()))
			ProjectileManager = TGGameMode->GetProjectileManager();

	OwnerSkeletalMeshComponent = Cast<USkeletalMeshComponent> (GetOwner()->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
	if(!OwnerSkeletalMeshComponent)
		UE_LOG(LogTemp, Error, TEXT("%s Component in %s actor failed to find SkeletalMeshComponent on owner."), *GetName(), *GetOwner()->GetName())
}


void UProjectileFiringComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

void UProjectileFiringComponent::TryToFire_DestinationLocation(const FVector& ProjectileFinalDestination_W)
{
	if(!OwnerSkeletalMeshComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("ProjectileFiringComponent doesn't have OwnerSkeletalMeshComponent variable set."));
		return;
	}

	const FVector FireSocketLocation_W = OwnerSkeletalMeshComponent->GetSocketLocation(FireSocketName);
	FVector NormalizedVectorToTarget_L = ProjectileFinalDestination_W - FireSocketLocation_W;
	NormalizedVectorToTarget_L.Normalize();

	FireProjectile
	(
		FireSocketLocation_W,
		NormalizedVectorToTarget_L.Rotation()
	);
}
void UProjectileFiringComponent::TryToFire_Rotation(const FRotator& ProjectileRotation)
{
	if(!OwnerSkeletalMeshComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("ProjectileFiringComponent doesn't have OwnerSkeletalMeshComponent variable set. (%s)"), *GetOwner()->GetName());
		return;
	}
	
	const FVector FireSocketLocation_W = OwnerSkeletalMeshComponent->GetSocketLocation(FireSocketName);

	FireProjectile
	(
		FireSocketLocation_W,
		ProjectileRotation
	);
}

void UProjectileFiringComponent::TryToFire_SocketDecisive()
{
	if(!OwnerSkeletalMeshComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("ProjectileFiringComponent doesn't have OwnerSkeletalMeshComponent variable set. (%s)"), *GetOwner()->GetName());
		return;
	}

	const FVector FireSocketLocation_W = OwnerSkeletalMeshComponent->GetSocketLocation(FireSocketName);
	const FRotator FireSocketRotation_W = OwnerSkeletalMeshComponent->GetSocketRotation(FireSocketName);

	FireProjectile(FireSocketLocation_W, FireSocketRotation_W);
}

void UProjectileFiringComponent::FireProjectile(const FVector& StartLocation_W, const FRotator& StartRotation_W) const
{
	if(!IsValid(ProjectileManager))
	{
		UE_LOG(LogTemp, Error, TEXT("ProjectileManager wasn't set in %s"), *GetOwner()->GetName());
		return;
	}
	
	AProjectileBase* ProjectileToFire = ProjectileManager->GetProjectile();
	if(!IsValid(ProjectileToFire))
	{
		UE_LOG(LogTemp, Error, TEXT("Failed to get Projectile from ProjectileManager for %s actor."), *GetOwner()->GetName());
		return;
	}
	
	APawn* ProjectileInstigator = Cast<APawn> (GetOwner()->GetInstigator());
	if(!ProjectileInstigator)
	{
		UE_LOG(LogTemp, Error, TEXT("Weapon instigator is not a Pawn (%s)."), *GetOwner()->GetName());
		return;
	}

	ProjectileToFire->SetActorLocationAndRotation(StartLocation_W, StartRotation_W);
	ProjectileToFire->FireProjectile(ProjectileInstigator, ProjectileInitialVelocity);
}

