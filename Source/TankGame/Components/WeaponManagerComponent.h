// Archi

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "WeaponManagerComponent.generated.h"

class UProjectileFiringComponent;
class UEnhancedInputLocalPlayerSubsystem;
class AWeaponBase;
class UInputMappingContext;
class UInputAction;

// Handles attaching weapon and adding an input for using it.
// Responsible for notifying the server that player wants to shoot.
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TANKGAME_API UWeaponManagerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UWeaponManagerComponent();
	
public:
	// TODO: Does it work properly??
	// Executed via interaction(BeginAction) on client and server. (Check InteractionManager for more info)
	// CAN'T BE USED IN CONSTRUCTOR.
	UFUNCTION(BlueprintCallable)
	void EquipWeapon(UProjectileFiringComponent* WeaponFiringComponent, bool bDisableCollision = true);
	
	// Called by input binding.
	UFUNCTION(BlueprintCallable, Server, Unreliable)
	void ServerTryToShoot();

protected:// EnhancedInput
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponManager | Enhanced Input")
	UInputMappingContext* WeaponMappingContext {nullptr};
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponManager | Enhanced Input")
	int32 WeaponMappingContextPriority {1};

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "WeaponManager | Enhanced Input")
	UInputAction* ShootInputAction {nullptr};

protected:// Setup variables
	// When marked WeaponManager will not check where the owning players is looking,
	// and instead fire the projectile with the Location and Rotation the socket defined in ProjectileFiringComponent has.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponManager | Setup")
	bool bUseSocketToDetermineProjectileSetup {false};

protected:// Runtime variables
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Debug | Variables")
	UProjectileFiringComponent* EquippedWeaponFiringComponent {nullptr};

protected:// Debug variables
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Debug ")
	bool bDrawDebugs {false};
	
protected:
	virtual void BeginPlay() override;

protected:
	UFUNCTION()
	void OnPawnPossessed(APawn* ChangedPawn);

	void SetupInput(APawn* PawnToSetupInput);
	
	// Assigns method to ShootInputAction.
	void RegisterInputActions(const APawn* OwningPawn);
};
