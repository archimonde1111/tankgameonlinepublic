// Copyright Epic Games, Inc. All Rights Reserved.


#include "TGGameMode.h"

#include "ProjectileManager.h"

#include "Misc/MessageDialog.h"

void ATGGameMode::BeginPlay()
{
	Super::BeginPlay();

	if(!ProjectileManagerClass)
	{
		UE_LOG(LogTemp, Error, TEXT("Failed to spawn ProjcetileManager, no base class provided."))
		FMessageDialog::Debugf(FText::FromString("Failed to spawn ProjectileManager, no base class provided"));
	}
	else
	{
		ProjectileManager = NewObject<UProjectileManager> (this, ProjectileManagerClass, FName("ProjectileManager"));
		ProjectileManager->Initialize();
	}
}

void ATGGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	ProjectileManager->ProjectileManagerTick(DeltaSeconds);
}