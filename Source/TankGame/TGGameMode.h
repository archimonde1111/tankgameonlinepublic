// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "TGGameMode.generated.h"

class UProjectileManager;
/**
 * 
 */
UCLASS()
class TANKGAME_API ATGGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	virtual void Tick(float DeltaSeconds) override;
	
public:
	UProjectileManager* GetProjectileManager() const
	{ return ProjectileManager; }
	
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings")
	TSubclassOf<UProjectileManager> ProjectileManagerClass = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Debug | Variables")
	UProjectileManager* ProjectileManager = nullptr;

protected:
	virtual void BeginPlay() override;
};
