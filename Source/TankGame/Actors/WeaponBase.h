// Archi

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TankGame/WeaponInterface.h"
#include "WeaponBase.generated.h"

class UInteractionComponent;
class UProjectileManager;
class UProjectileFiringComponent;

UENUM()
enum class EWeaponState : uint8
{
	EUnUsed UMETA(DisplayName = "UnUsed"),
	EInUse UMETA(DisplayName = "InUse")
};

UCLASS()
class TANKGAME_API AWeaponBase : public AActor, public IWeaponInterface
{
	GENERATED_BODY()

public:// Components.
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings | Components")
	USkeletalMeshComponent* WeaponSkeletalMesh {nullptr};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings | Components")
	UInteractionComponent* InteractionComponent {nullptr};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings | Components")
	UProjectileFiringComponent* ProjectileFiringComponent {nullptr};
	
public:// Setup variables.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings | Setup")
	FName WeaponAttachmentSocket = FName();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings | Setup")
	TSubclassOf<UAnimInstance> WeaponAnimationInstance;
	
public:	
	AWeaponBase();
	virtual void Tick(float DeltaTime) override;

public:// Interface methods
	virtual void OnWeaponEquipped_Implementation(AActor* EquippingActor, const bool bDisableCollision) override;
	
	/*virtual void TryToFire_Location_Implementation(const AActor* owningActor, const FVector& ProjectileFinalDestination_W);
	virtual void TryToFire_Rotation_Implementation(const AActor* owningActor, const FRotator& ProjectileRotation);*/

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Debug | Variables")
	EWeaponState WeaponState {EWeaponState::EUnUsed};
	
protected:
	virtual void BeginPlay() override;

};
