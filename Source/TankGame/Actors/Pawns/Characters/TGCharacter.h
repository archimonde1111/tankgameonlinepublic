// Archi

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Character.h"
#include "PlayerPawnInterface.h"

#include "TGCharacter.generated.h"


UENUM()
enum class EGameplayType : uint8
{
	EGT_ThirdPerson,
	EGT_ThirdPersonShooting,
	EGT_MAX
};


class UEnhancedInputLocalPlayerSubsystem;

class UWeaponManagerComponent;
class USpringArmComponent;
class UCameraComponent;
class UAbilitySystemComponent;

class UInputAction;
class UInputMappingContext;
struct FInputActionValue;

class UInteractionManager;

// TODO: Create component that will handle adding mapping contexts and InputActions to pawns and so on..
UCLASS()
class TANKGAME_API ATGCharacter : public ACharacter, public IPlayerPawnInterface
{
	GENERATED_BODY()
	
protected:// Base components
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "TankGameCharacter | Components")
	USpringArmComponent* SpringArmComponent {nullptr};
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "TankGameCharacter | Components")
	UCameraComponent* CameraComponent {nullptr};

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "TankGameCharacter | Components")
	UInteractionManager* InteractionManager {nullptr};

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "TankGameCharacter | Components")
	UWeaponManagerComponent* WeaponManager {nullptr};

protected:// EnhancedInput
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TankGameCharacter | Enhanced Input")
	UInputMappingContext* BaseMappingContext {nullptr};
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TankGameCharacter | Enhanced Input")
	int32 BaseMappingContextPriority {1};

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TankGameCharacter | Enhanced Input")
	UInputAction* MovementAction {nullptr};
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TankGameCharacter | Enhanced Input")
	UInputAction* LookTurnAction {nullptr};
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TankGameCharacter | Enhanced Input")
    UInputAction* JumpAction {nullptr};
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TankGameCharacter | Enhanced Input")
	UInputAction* InteractAction {nullptr};

public:// IPlayerCharacterInterface interface methods
	virtual const UCameraComponent* GetPlayerCamera() override
	{
		return CameraComponent;
	}
	
public:// Engine methods
	ATGCharacter();
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void Tick(float DeltaTime) override;

public:// Player viewpoint
	void ChangeGameplayType(EGameplayType GameplayType);

protected:
	void ChangeGameplayType_ThirdPerson();
	void ChangeGameplayType_ThirdPersonShooting();
	
protected:// EnhancedInput
	// Called from OnPossessed but executes on client.
	virtual void PawnClientRestart() override;
	UEnhancedInputLocalPlayerSubsystem* GetEnhancedInputLocalPlayerSubsystem();
	// Clears EnhancedInputSubsystem from all mapping contexts.
	void ClearAllMappingContexts(UEnhancedInputLocalPlayerSubsystem* EnhancedInputSubsystem) const;
	// Adds BaseMappingContext to EnhancedInputSubsystem.
	void AddMappingContexts(UEnhancedInputLocalPlayerSubsystem* EnhancedInputSubsystem) const;
	void AssignMethodsToInputActions(UInputComponent* PlayerInputComponent);
	
	void EnhancedInput_Movement(const FInputActionValue& Value);
	void EnhancedInput_LookTurn(const FInputActionValue& Value);
	void EnhancedInput_Jump(const FInputActionValue& Value);
	void EnhancedInput_Interact(const FInputActionValue& Value);
	
protected:
	virtual void BeginPlay() override;
	
};
