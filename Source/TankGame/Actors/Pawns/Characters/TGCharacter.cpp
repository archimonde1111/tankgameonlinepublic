// Archi


#include "TGCharacter.h"

#include "EnhancedInputSubsystems.h"
#include "EnhancedInputComponent.h"
#include "InputActionValue.h"

#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"

#include "InteractionManager.h"

#include "TankGame/Components/WeaponManagerComponent.h"


ATGCharacter::ATGCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	// Rotate spring arm with control rotation(player mouse movement).
	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent> (FName("SpringArmComponent"));
	SpringArmComponent->SetupAttachment(GetRootComponent());

	CameraComponent = CreateDefaultSubobject<UCameraComponent> (FName("CameraComponent"));
	CameraComponent->SetupAttachment(SpringArmComponent, USpringArmComponent::SocketName);

	// Interaction plugin.
	InteractionManager = CreateDefaultSubobject<UInteractionManager> (FName("InteractionManager"));

	// Weapon manager.
	WeaponManager = CreateDefaultSubobject<UWeaponManagerComponent> (FName("WeaponManager"));

	ChangeGameplayType(EGameplayType::EGT_ThirdPerson);
}

void ATGCharacter::ChangeGameplayType(EGameplayType GameplayType)
{
	switch (GameplayType)
	{
	case EGameplayType::EGT_ThirdPerson:
		ChangeGameplayType_ThirdPerson();
		break;
	case EGameplayType::EGT_ThirdPersonShooting:
		ChangeGameplayType_ThirdPersonShooting();
		break;
	default:
		ensureAlwaysMsgf(false, TEXT("ChangeGameplayType recieved unhandled enum %i(index)"), GameplayType);
	}
}
void ATGCharacter::ChangeGameplayType_ThirdPerson()
{
	// Don't rotate character with camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
	bUseControllerRotationYaw = false;

	// Setup rotation to movement vector.
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 500.f, 0.f);

	SpringArmComponent->bUsePawnControlRotation = true;
	
	CameraComponent->bUsePawnControlRotation = false;
}
void ATGCharacter::ChangeGameplayType_ThirdPersonShooting()
{
	// Rotate character with camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false; 
	bUseControllerRotationYaw = true;

	GetCharacterMovement()->bOrientRotationToMovement = false;

	SpringArmComponent->bUsePawnControlRotation = true;
	SpringArmComponent->bInheritPitch = true;
	SpringArmComponent->bInheritRoll = true;
	SpringArmComponent->bInheritYaw = true;
	
	CameraComponent->bUsePawnControlRotation = false;
}

void ATGCharacter::BeginPlay()
{
	Super::BeginPlay();

	if(HasAuthority())
	{
		SetReplicates(true);
		SetReplicateMovement(true);
	}
}


void ATGCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}


void ATGCharacter::PawnClientRestart()
{
	UEnhancedInputLocalPlayerSubsystem* EnhancedInputSubsystem = GetEnhancedInputLocalPlayerSubsystem();
	if(!EnhancedInputSubsystem)
	{
		UE_LOG(LogTemp, Error,
			TEXT("FAILED to get EnhancedInputSubsystem, input system won't work. Check if you have enabled EnhancedInputSystem in project settings"));
	}
	
	// Clearing mapping contexts before Super call to make sure things like WeaponManagerComponent won't add own mapping contexts only to have them deleted later.
	ClearAllMappingContexts(EnhancedInputSubsystem);

	Super::PawnClientRestart();

	AddMappingContexts(EnhancedInputSubsystem);
}
UEnhancedInputLocalPlayerSubsystem* ATGCharacter::GetEnhancedInputLocalPlayerSubsystem()
{
	if(const APlayerController* PlayerController = Cast<APlayerController> (GetController()))
	{
		// EnhancedInput subsystem is located in local player related to PlayerController
		return ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem> (PlayerController->GetLocalPlayer());
	}

	return nullptr;
}
void ATGCharacter::ClearAllMappingContexts(UEnhancedInputLocalPlayerSubsystem* EnhancedInputSubsystem) const
{
	if(EnhancedInputSubsystem)
	{
		// PawnClientRestart can run more than once in Actor's lifetime, clearing all leftovers
		EnhancedInputSubsystem->ClearAllMappings();
	}
}
void ATGCharacter::AddMappingContexts(UEnhancedInputLocalPlayerSubsystem* EnhancedInputSubsystem) const
{
	if(EnhancedInputSubsystem)
	{
		// Add MappingContexts along with Priority.
		EnhancedInputSubsystem->AddMappingContext(BaseMappingContext, BaseMappingContextPriority);
	}
}


void ATGCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	AssignMethodsToInputActions(PlayerInputComponent);
}
void ATGCharacter::AssignMethodsToInputActions(UInputComponent* PlayerInputComponent)
{
	if(UEnhancedInputComponent* PlayerEnhancedInputComponent = Cast<UEnhancedInputComponent> (PlayerInputComponent))
	{
		if(MovementAction)
			PlayerEnhancedInputComponent->BindAction(MovementAction, ETriggerEvent::Triggered, this, &ATGCharacter::EnhancedInput_Movement);
		if(LookTurnAction)
			PlayerEnhancedInputComponent->BindAction(LookTurnAction, ETriggerEvent::Triggered, this, &ATGCharacter::EnhancedInput_LookTurn);
		if(JumpAction)
			PlayerEnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Started, this, &ATGCharacter::EnhancedInput_Jump);
		if(InteractAction)
			PlayerEnhancedInputComponent->BindAction(InteractAction, ETriggerEvent::Started, this, &ATGCharacter::EnhancedInput_Interact);
	}
}

void ATGCharacter::EnhancedInput_Movement(const FInputActionValue& Value)
{
	if(Value.GetMagnitude() == 0.f)
		return;

	const FRotator YawRotation(0, GetControlRotation().Yaw, 0);
	const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
	const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	
	AddMovementInput(ForwardDirection, Value[0]);// Get values from W and S
	AddMovementInput(RightDirection , Value[1]);// Get values from A and D
}

void ATGCharacter::EnhancedInput_LookTurn(const FInputActionValue& Value)
{
	AddControllerYawInput(Value[0]);// Get mouse X axis
	AddControllerPitchInput(Value[1]);// Get mouse Y axis
}

void ATGCharacter::EnhancedInput_Jump(const FInputActionValue& Value)
{
	Jump();
}

void ATGCharacter::EnhancedInput_Interact(const FInputActionValue& Value)
{
	InteractionManager->Server_PerformInteraction();
}
