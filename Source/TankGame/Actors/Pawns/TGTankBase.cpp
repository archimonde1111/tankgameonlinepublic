// Archi


#include "TGTankBase.h"

#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"

#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "ChaosWheeledVehicleMovementComponent.h"
#include "InteractionComponent.h"
#include "InteractionManager.h"
#include "Net/UnrealNetwork.h"

#include "TankGame/Components/ProjectileFiringComponent.h"
#include "TankGame/Components/WeaponManagerComponent.h"


ATGTankBase::ATGTankBase()
{
	PrimaryActorTick.bCanEverTick = true;

	// Don't rotate tank with camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
	bUseControllerRotationYaw = false;
	
	TankSkeletalMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent> (FName("TankSkeletalMeshComponent"));
	SetRootComponent(TankSkeletalMeshComponent);

	// Attached in PostInitProperties to make sure CommanderHatchSocketName is filled with data from BP.
	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent> (FName("SpringArmComponent"));

	CameraComponent = CreateDefaultSubobject<UCameraComponent> (FName("CameraComponent"));
	CameraComponent->SetupAttachment(SpringArmComponent, USpringArmComponent::SocketName);

	// Rotate spring arm with control rotation(player mouse movement).
	// Turret rotation works based on client input 
	SpringArmComponent->bUsePawnControlRotation = true;
	SpringArmComponent->bInheritPitch = true;
	SpringArmComponent->bInheritRoll = true;
	SpringArmComponent->bInheritYaw = true;
	CameraComponent->bUsePawnControlRotation = false;
	
	
	ChaosVehicleMovementComponent = CreateDefaultSubobject<UChaosWheeledVehicleMovementComponent> (FName("ChaosVehicleMovementComponent"));
	ChaosVehicleMovementComponent->SetIsReplicated(true);

	WeaponManager = CreateDefaultSubobject<UWeaponManagerComponent> (FName("WeaponManager"));
	ProjectileFiringComponent = CreateDefaultSubobject<UProjectileFiringComponent> (FName("ProjectileFiringComponent"));

	InteractionManager = CreateDefaultSubobject<UInteractionManager> (FName("InteractionManager"));
	InteractionManager->bIgnoreOwningActor = false;
	InteractionComponent = CreateDefaultSubobject<UInteractionComponent> (FName("InteractionComponent"));
}

void ATGTankBase::PostInitProperties()
{
	Super::PostInitProperties();

	SpringArmComponent->SetupAttachment(GetRootComponent(), CommanderHatchSocketName);
}

void ATGTankBase::BeginPlay()
{
	Super::BeginPlay();

	// For some reason the variable always changes to OnlyTickPoseWhenRendered when game starts.
	// Changed so server executes Tank's ABP(turret and barrel rotation).
	TankSkeletalMeshComponent->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::AlwaysTickPoseAndRefreshBones;
	
	WeaponManager->EquipWeapon(ProjectileFiringComponent);
}


void ATGTankBase::PawnClientRestart()
{
	Super::PawnClientRestart();

	UEnhancedInputLocalPlayerSubsystem* EnhancedInputSubsystem {nullptr};
	if(const APlayerController* PlayerController = Cast<APlayerController> (GetController()))
	{
		EnhancedInputSubsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem> (PlayerController->GetLocalPlayer());
		// EnhancedInput subsystem is located in local player related to PlayerController
		if(EnhancedInputSubsystem)
		{
			// PawnClientRestart can run more than once in Actor's lifetime, clearing all leftovers
			EnhancedInputSubsystem->ClearAllMappings();
		}
		else
		{
			return;
		}
	}
	else
	{
		return;
	}
	
	for(const auto& InputMappingContext : TankInputMappingContexts)
	{
		if(!InputMappingContext.Key)
		{
			return;
		}

		// Add MappingContexts along with Priority.
		EnhancedInputSubsystem->AddMappingContext(InputMappingContext.Key, InputMappingContext.Value);
	}

	APlayerController* PlayerController = Cast<APlayerController> (GetController());
	PlayerController->SetControlRotation(FRotator(GetActorRotation().Pitch + TargetBarrelRotationPitch, GetActorRotation().Yaw + TargetTurretRotationYaw, 0.f));
}

void ATGTankBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// TODO: Add Timer to resend the event, just in case unreliable failed? Or make it reliable?
	if(bTargetRotationChanged)
	{
		Server_TrySendTargetPlayerTurretRotation(GetControlRotation().Yaw, GetControlRotation().Pitch);
		bTargetRotationChanged = false;
	}
}

void ATGTankBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	if(UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent> (PlayerInputComponent))
	{
		if(MovementAction)
		{
			EnhancedInputComponent->BindAction(MovementAction, ETriggerEvent::Triggered, this, &ATGTankBase::EnhancedInput_Movement);
			EnhancedInputComponent->BindAction(MovementAction, ETriggerEvent::Completed, this, &ATGTankBase::EnhancedInput_MovementFinished);
		}
		if(LookTurnAction)
		{
			EnhancedInputComponent->BindAction(LookTurnAction, ETriggerEvent::Triggered, this, &ATGTankBase::EnhancedInput_LookTurn);
		}
		if(InteractAction)
		{
			EnhancedInputComponent->BindAction(InteractAction, ETriggerEvent::Started, this, &ATGTankBase::EnhancedInput_Interact);
		}
	}
}

void ATGTankBase::EnhancedInput_Movement(const FInputActionValue& Value)
{
	if(Value[1] >= 0)
	{
		ChaosVehicleMovementComponent->SetThrottleInput(Value[1]);
	}
	else if(Value[1] < 0)
	{
		ChaosVehicleMovementComponent->SetBrakeInput(Value[1] * -1);
	}

	ChaosVehicleMovementComponent->SetYawInput(Value[0]);
}
void ATGTankBase::EnhancedInput_MovementFinished(const FInputActionValue& Value)
{
	ChaosVehicleMovementComponent->SetThrottleInput(0);
	ChaosVehicleMovementComponent->SetBrakeInput(0);
	ChaosVehicleMovementComponent->SetYawInput(0);
}

void ATGTankBase::EnhancedInput_LookTurn(const FInputActionValue& Value)
{
	// Value[0] == mouse X && Value[1] == mouse Y

	bTargetRotationChanged = true;
	
	AddControllerYawInput(Value[0]);
	AddControllerPitchInput(Value[1]);
	
	TargetTurretRotationYaw = GetControlRotation().Yaw;
	TargetBarrelRotationPitch = GetControlRotation().Pitch < 180 ? GetControlRotation().Pitch : GetControlRotation().Pitch - 360;
}

void ATGTankBase::EnhancedInput_Interact(const FInputActionValue& Value)
{
	InteractionManager->Server_PerformInteraction();
}

void ATGTankBase::Server_TrySendTargetPlayerTurretRotation_Implementation(const float InClientControlRotationYaw, const float InClientControlRotationPitch)
{
	GetController()->SetControlRotation(FRotator(InClientControlRotationPitch, InClientControlRotationYaw, 0.f));
	
	TargetTurretRotationYaw = InClientControlRotationYaw;
	TargetBarrelRotationPitch = InClientControlRotationPitch < 180 ? InClientControlRotationPitch : InClientControlRotationPitch - 360;
}

// Replicating from server to SimulatedProxy.
void ATGTankBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATGTankBase, TargetTurretRotationYaw);
	DOREPLIFETIME(ATGTankBase, TargetBarrelRotationPitch);
}