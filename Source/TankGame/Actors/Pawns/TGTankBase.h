// Archi

#pragma once

#include "CoreMinimal.h"
#include "PlayerPawnInterface.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/SpringArmComponent.h"

#include "TGTankBase.generated.h"

class UInteractionManager;
class UInputMappingContext;
class USpringArmComponent;
class UCameraComponent;
class UWeaponManagerComponent;
class UProjectileFiringComponent;
class UInteractionComponent;

class UInputAction;
class UChaosWheeledVehicleMovementComponent;

struct FInputActionValue;

UCLASS()
class TANKGAME_API ATGTankBase : public APawn, public IPlayerPawnInterface
{
	GENERATED_BODY()

public:
	ATGTankBase();
	virtual void PostInitProperties() override;

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;
	virtual void Tick(float DeltaTime) override;

public:
	virtual const UCameraComponent* GetPlayerCamera() override
	{ return CameraComponent; }
	
protected:// Base components
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TankBase | BaseComponents")
	USkeletalMeshComponent* TankSkeletalMeshComponent {nullptr};

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TankBase | BaseComponents")
	USpringArmComponent* SpringArmComponent {nullptr};
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TankBase | BaseComponents")
	UCameraComponent* CameraComponent {nullptr};
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TankBase | BaseComponents")
	UWeaponManagerComponent* WeaponManager {nullptr};
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TankBase | BaseComponents")
	UProjectileFiringComponent* ProjectileFiringComponent {nullptr};

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TankBase | BaseComponents")
	UInteractionManager* InteractionManager {nullptr};
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TankBase | BaseComponents")
	UInteractionComponent* InteractionComponent {nullptr};

protected:// Chaos vehicle movement component
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TankBase | BaseComponents")
	UChaosWheeledVehicleMovementComponent* ChaosVehicleMovementComponent {nullptr};
	
protected:// EnhancedInput
	// Input mapping context and it's priority.
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, EditFixedSize, Category = "TankBase | Enhanced Input")
	TMap<UInputMappingContext*, int32> TankInputMappingContexts {TPair<UInputMappingContext*, float> (nullptr, 1)};

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TankBase | Enhanced Input")
	UInputAction* MovementAction {nullptr};
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TankBase | Enhanced Input")
	UInputAction* LookTurnAction {nullptr};
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TankBase | Enhanced Input")
	UInputAction* InteractAction {nullptr};

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TankBase | Setup")
	FName BarrelEndSocketName;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "TankBase | Setup")
	FName CommanderHatchSocketName;
	
	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "TankBase | Debug")
	float TargetTurretRotationYaw {0.f};
	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "TankBase | Debug")
	float TargetBarrelRotationPitch {0.f};
	
protected:
	virtual void BeginPlay() override;

protected:// EnhancedInput
	// Called from OnPossessed but executes on client.
	virtual void PawnClientRestart() override;

	void EnhancedInput_Movement(const FInputActionValue& Value);
	void EnhancedInput_MovementFinished(const FInputActionValue& Value);
	void EnhancedInput_LookTurn(const FInputActionValue& Value);
	void EnhancedInput_Interact(const FInputActionValue& Value);

protected:// Handling player input

	bool bTargetRotationChanged {false};
	
	// If marked as Unreliable YawInput and PitchInput will need some system to check server and client states and fix if needed.
	UFUNCTION(Server, Reliable)
	void Server_TrySendTargetPlayerTurretRotation(const float InClientControlRotationYaw, const float InClientControlRotationPitch);
};
