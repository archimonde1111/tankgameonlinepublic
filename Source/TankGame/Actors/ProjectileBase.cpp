// Archi


#include "ProjectileBase.h"

#include "GameFramework/ProjectileMovementComponent.h"


AProjectileBase::AProjectileBase()
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = false;

	ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent> (FName("BulletMesh"));
	SetRootComponent(ProjectileMesh);
	
	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent> (FName("ProjectileMovementComponent"));
	ProjectileMovementComponent->bAutoActivate = false;
	ProjectileMovementComponent->OnComponentActivated.AddDynamic(this, &AProjectileBase::OnProjectileFired);
	ProjectileMovementComponent->OnProjectileStop.AddDynamic(this, &AProjectileBase::OnProjectileHit);
	
	DisableProjectile_Replication();
	DisableProjectile_Physics();
}

void AProjectileBase::BeginPlay()
{
	Super::BeginPlay();
}


void AProjectileBase::FireProjectile(APawn* ProjectileInstigator, const float ProjectileVelocity)
{
	SetInstigator(ProjectileInstigator);
	
	EnableProjectile_Replication();
	EnableProjectile_Physics();

	// UpdatedComponent is changed to null when projectile hits something so we need to set it back.
	if(!ProjectileMovementComponent->UpdatedComponent)
	{
		ProjectileMovementComponent->SetUpdatedComponent(GetRootComponent());
	}
	ProjectileMovementComponent->SetVelocityInLocalSpace(ProjectileForwardVector * ProjectileVelocity);
	ProjectileMovementComponent->Activate(true);
}


void AProjectileBase::DisableProjectile_Replication()
{
	if(ProjectileTimerHandle.IsValid())
		GetWorldTimerManager().ClearTimer(ProjectileTimerHandle);
	
	SetReplicates(false);
	ProjectileMesh->SetIsReplicated(false);
}
void AProjectileBase::DisableProjectile_Physics()
{
	if(ProjectileTimerHandle.IsValid())
		GetWorldTimerManager().ClearTimer(ProjectileTimerHandle);
	
	ProjectileMesh->SetVisibility(false);
	ProjectileMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	ProjectileMesh->SetNotifyRigidBodyCollision(false);
}
void AProjectileBase::EnableProjectile_Replication()
{
	SetReplicates(true);
	ProjectileMesh->SetIsReplicated(true);
}
void AProjectileBase::EnableProjectile_Physics()
{
	ProjectileMesh->SetVisibility(true);
	ProjectileMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	ProjectileMesh->SetNotifyRigidBodyCollision(true);
}


void AProjectileBase::OnProjectileFired(UActorComponent* Component, bool bReset)
{
	// Setting timer so projectile won't fly for ever(or at least until UE decides to delete it).
	GetWorldTimerManager().SetTimer(ProjectileTimerHandle, this, &AProjectileBase::OnProjectileTimedOut, ProjectileTimeout);
}
void AProjectileBase::OnProjectileTimedOut()
{
	DisableProjectile_Physics();
	OnProjectileTimeout.Broadcast(this, GetInstigator());
	
	SetInstigator(nullptr);
}

void AProjectileBase::OnProjectileHit(const FHitResult& ImpactResult)
{
	//Server only.
	GetWorldTimerManager().ClearTimer(ProjectileTimerHandle);

	// Visuals spawn.
	OnProjectileHit_Multicast(ImpactResult);
	
	DisableProjectile_Physics();
	// TODO: When replication is turned off 
	OnProjectileHitDelegate.Broadcast(this, GetInstigator(), ImpactResult);
	//DisableProjectile_Replication();

	SetInstigator(nullptr);
}
void AProjectileBase::OnProjectileHit_Multicast_Implementation(const FHitResult& ImpactResult)
{
	// Server and client. (ListenServer game)
	BP_OnProjectileHit_Visual(ImpactResult);
}

