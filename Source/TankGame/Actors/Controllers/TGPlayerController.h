// Archi

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TGPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class TANKGAME_API ATGPlayerController : public APlayerController
{
	GENERATED_BODY()
};
