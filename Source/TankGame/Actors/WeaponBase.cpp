// Archi

#include "WeaponBase.h"

#include "Components/SkeletalMeshComponent.h"

#include "InteractionComponent.h"
#include "Pawns/Characters/TGCharacter.h"
#include "TankGame/Components/ProjectileFiringComponent.h"


AWeaponBase::AWeaponBase()
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;
	
	WeaponSkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent> (FName("WeaponSkeletalMesh"));
	SetRootComponent(WeaponSkeletalMesh);

	InteractionComponent = CreateDefaultSubobject<UInteractionComponent> (FName("InteractionComponent"));

	ProjectileFiringComponent = CreateDefaultSubobject<UProjectileFiringComponent> (FName("ProjectileFiringComponent"));
}

void AWeaponBase::BeginPlay()
{
	Super::BeginPlay();

#if WITH_EDITOR
	if(WeaponAttachmentSocket == FName())
	{
		UE_LOG(LogTemp, Warning, TEXT("Weapon %s doesn't have WeaponAttachmentSocket variable set."))
	}
#endif
}

void AWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWeaponBase::OnWeaponEquipped_Implementation(AActor* EquippingActor, const bool bDisableCollision)
{
	ATGCharacter* EquippingCharacter = Cast<ATGCharacter> (EquippingActor);
	if(!EquippingCharacter)
		return;
	
	WeaponSkeletalMesh->SetSimulatePhysics(false);
	if(bDisableCollision)
		WeaponSkeletalMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	SetOwner(EquippingCharacter);
	SetInstigator(EquippingCharacter);
	AttachToComponent
	(
		EquippingCharacter->GetMesh(),
		FAttachmentTransformRules::SnapToTargetNotIncludingScale,
		WeaponAttachmentSocket
	);

	// Change AnimationBlueprint to one that have animations for specific weapon.
	EquippingCharacter->GetMesh()->SetAnimInstanceClass(WeaponAnimationInstance);
	EquippingCharacter->ChangeGameplayType(EGameplayType::EGT_ThirdPersonShooting);
}

/*void AWeaponBase::TryToFire_Location_Implementation(const AActor* owningActor, const FVector& ProjectileFinalDestination_W)
{
	if(GetOwner() != owningActor)
		return;

	const FVector FireSocketLocation_W = WeaponSkeletalMesh->GetSocketLocation(FireSocketName);
	FVector NormalizedVectorToTarget_L = ProjectileFinalDestination_W - FireSocketLocation_W;
	NormalizedVectorToTarget_L.Normalize();

	ProjectileFiringComponent->FireProjectile
	(
		ProjectileInitialVelocity,
		FireSocketLocation_W,
		NormalizedVectorToTarget_L.Rotation()
	);
}
void AWeaponBase::TryToFire_Rotation_Implementation(const AActor* owningActor, const FRotator& ProjectileRotation)
{
	if(GetOwner() != owningActor)
		return;
	
	const FVector FireSocketLocation_W = WeaponSkeletalMesh->GetSocketLocation(FireSocketName);

	ProjectileFiringComponent->FireProjectile
	(
		ProjectileInitialVelocity,
		FireSocketLocation_W,
		ProjectileRotation
	);
}*/

