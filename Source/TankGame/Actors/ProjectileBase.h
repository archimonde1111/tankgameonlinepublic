// Archi

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProjectileBase.generated.h"

class UProjectileMovementComponent;
class AProjectileBase;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams
(
	FOnProjectileHit,
	AProjectileBase*, Projectile,
	APawn*, ProjectileInstigator,
	const FHitResult&, ImpactResult
);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams
(
	FOnProjectileTimeout,
	AProjectileBase*, Projectile,
	APawn*, ProjectileInstigator
);

UCLASS()
class TANKGAME_API AProjectileBase : public AActor
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings")
	FVector ProjectileForwardVector {1, 0, 0};
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings")
	float ProjectileTimeout {5.f};

	// Created because OnProjectileStop from ProjectileMovementComponent doesn't give instigator and Manager needs it.
	UPROPERTY(BlueprintAssignable, Category = "ProjectileBase")
	FOnProjectileHit OnProjectileHitDelegate;
	UPROPERTY(BlueprintAssignable, Category = "ProjectileBase")
	FOnProjectileTimeout OnProjectileTimeout;
	
public:// Engine methods.
	AProjectileBase();

public:// Projectile methods
	void FireProjectile(APawn* ProjectileInstigator, const float ProjectileVelocity);

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Components")
	UStaticMeshComponent* ProjectileMesh {nullptr};
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Components")
	UProjectileMovementComponent* ProjectileMovementComponent {nullptr};
	
	FTimerHandle ProjectileTimerHandle;

/*
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Debug")
	const AActor* CurrentInstigator {nullptr};*/

protected:// Enable/Disable methods
	// Methods with _Replication postfix disable all replication on/of projectile.
	// Methods with _Visual postfix disable visibility of projectile as well as physics.
	void EnableProjectile_Replication();
	void EnableProjectile_Physics();
	// TODO: Check if keeping projectiles replicated encumbers(obciąża) bandwidth.
	// Turning off replication every hit might be not the best idea as client might have to respawn the projectile TODO: check.
	void DisableProjectile_Replication();
	void DisableProjectile_Physics();
	
protected:// Engine methods
	virtual void BeginPlay() override;

protected:
	// Called on server.
	UFUNCTION()
	void OnProjectileHit(const FHitResult& ImpactResult);

	UFUNCTION(NetMulticast, Unreliable, Category = "ProjectileBase")
	void OnProjectileHit_Multicast(const FHitResult& ImpactResult);
	// Only for visual events. Not marked as BlueprintCosmetic because game is meant to be a ListenServer.
	UFUNCTION(BlueprintImplementableEvent, Category = "ProjectileBase")
	void BP_OnProjectileHit_Visual(const FHitResult& ImpactResult);

	UFUNCTION()
	void OnProjectileFired(UActorComponent* Component, bool bReset);
	UFUNCTION()
	void OnProjectileTimedOut();
};


