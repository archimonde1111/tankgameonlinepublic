// Archi

#pragma once

#include "CoreMinimal.h"
#include "ProjectileManager.generated.h"

class AProjectileBase;

/**
 * TODO: Add projectile deletion if not used for some time.
 * Singleton??
 * TODO: Add ability to spawn more than 1 type of projectile.
 * 
 * Keeps pool of bullets to use so we can avoid constant actor spawning and deleting.
 *
 * Spawned only on server.
 */
UCLASS(Blueprintable)
class TANKGAME_API UProjectileManager : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settigns")
	TSubclassOf<AProjectileBase> ProjectileBaseClass {nullptr};

	// WARNING: Try not to go over the edge with this number as above 10^4 might cause jitter when starting the game.
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settigns", meta = (ClampMin = "0"))
	int32 BaseNumberOfProjectiles {100};

public:
	void Initialize();
	void ProjectileManagerTick(const float DeltaTime);

	UFUNCTION()
	AProjectileBase* GetProjectile();
	
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Debug | Projectiles")
	TArray<AProjectileBase*> FreeProjectiles {};
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Debug | Projectiles")
	TArray<AProjectileBase*> EngagedProjectiles {};

protected:
	UFUNCTION()
	void OnProjectileHit(AProjectileBase* InstigatorProjectile, APawn* ProjectileInstigator, const FHitResult& ImpactResult);
	UFUNCTION()
	void OnProjectileTimeout(AProjectileBase* Projectile, APawn* ProjectileInstigator);
	UFUNCTION()
	AProjectileBase* SpawnProjectile() const;
};
